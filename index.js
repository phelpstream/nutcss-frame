const { parse, parseBase } = require("./parse")

const frame = {
  frame: {
    alt: ["parse", "scrape"],
    fn: parse
  },
  parseBase: {
    fn: query => {
      return parseBase
    }
  }
}

module.exports = frame
