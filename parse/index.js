//@ts-check
const { __, type, trim, is, equals, clone, not, find, and, isEmpty } = require("ramda")
const { eachIndexedFlip, digStop, getOr, get, has, lower, Stop, each, eachIndexed } = require("svpr") //dig,

function parseBase(properties, digFn = query => (v, k) => v) {
  function detectProperties(value) {
    let result = {
      value,
      isArray: is(Array, value),
      isEmpty: isEmpty(value),
      isObject: is(Object, value),
      isString: is(String, value),
      anyOf: false
    }
    eachIndexedFlip(properties, (propertyNameAlternatives, property) => {
      let foundProperty = find(has(__, value), propertyNameAlternatives)
      if (and(foundProperty, not(result.anyOf))) result.anyOf = true
      result[property] = {
        exists: !!foundProperty,
        value: !!foundProperty ? value[foundProperty] : undefined,
        type: !!foundProperty ? type(value[foundProperty]) : undefined
      }
    })
    return result
  }

  return function(query, frame) {
    let rootFrame = { root: frame }
    query.set(
      digStop((val, key) => {
        let parsedVal = detectProperties(val)
        return digFn(query)(parsedVal, key)
      }, rootFrame).root || {}
    )
    return query
  }
}

const properties = {
  $selector: ["$s", "$selector"],
  $data: ["$d", "$data"],
  $actions: ["$a", "$actions"],
  $group: ["$g", "$group"],
  $key: ["$k", "$key"]
}
const recursiveParseFn = query => (props, key) => {
  const $ = query.$
  if (props.isArray && !props.isEmpty) {
    return Stop(query.nut(`${get("0", props.value)} @each text`).value())
  } else if (props.isObject) {
    if (props.$selector.exists) {
      let q = query.nut(props.$selector.value)
      if (props.$actions.exists) {
        if (is(Array, props.$actions.value)) {
          let actionsAsString = ""
          each(action => {
            if (is(String, action)) {
              actionsAsString = `${actionsAsString} ${action}`
            } else if (is(Object, action)) {
              eachIndexed((itActionValue, itActionParams) => {
                if (is(String, itActionValue)) actionsAsString = `${actionsAsString} ${itActionValue}(${itActionParams})`
                else if (is(Array, itActionValue)) actionsAsString = `${actionsAsString} ${itActionValue}(${itActionParams.join(")(")})`
              }, action)
            }
          }, props.$actions.value)
          q.nut(`@ ${actionsAsString}`)
        } else if (is(String, props.$actions.value)) q.nut(`@ ${props.$actions.value}`)
      }
      if (props.$data.exists) {
        let localFrame = {}
        let isList = false
        if (equals("Array", props.$data.type)) {
          isList = true
          localFrame = get("value.0", props.$data)
        } else if (equals("Object", props.$data.type)) {
          localFrame = get("value", props.$data)
          if (q.length > 1) q = q.eq(0) // take only the first one if selector didn't specify
        }
        q.each(qLocal => parse(qLocal, localFrame).value(), { saveData: true })
        if (!isList) q.set(get("0", q.value()))
      }
      return Stop(q.value())
    }
    return props.value
  } else if (props.isString) {
    return Stop(query.nut(props.value).value())
  }
}

const parse = parseBase(properties, recursiveParseFn)

module.exports = { parse, parseBase }
