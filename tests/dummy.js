const nutcss = require("nutcss")
const framePlugin = require("../index")
const fs = require("fs")
const path = require("path")

nutcss.extend({
  prototypes: framePlugin
})

nutcss.extend({
  actions: {
    lookupFrame: {
      fn: (query, frame) => {
        const parseBase = query.parseBase()
        const properties = {}
        if (parseBase)
          return parseBase(properties, query => (props, key) => {
            return props.value
          })(query, frame)
        query.set(null)
        return query
      }
    }
  }
})

let html = fs.readFileSync(path.join(__dirname, "./thedummy.html")).toString()
let $ = nutcss.load(html)

// let frame = {
//   $s: "li.item",
//   $d: {
//     name: "span.planName",
//     price: "span.planPrice"
//   }
// }

let frame = {
  $s: "li.item",
  $d: {
    name: "span.planName",
    price: "span.planPrice"
  }
}

let result = $("html")
  .parse(frame)
  .value()

// let result = $("html")
//   .parse({
//     title: "title",
//     popup: {
//       value: ".popup span"
//     }
//   })
//   .value()

// let result = $("html")
//   .lookupFrame({
//     plan: {
//       name: "hacker",
//       price: "free"
//     }
//   })
//   .value()

console.log(result)

// let result2 = $("html")
//   .parse({
//     title: "title @ uniqueSelector"
//   })
//   .value()

// console.log(result2)

// let result = $("html")
//   .nut("title")
//   .value()
